package cl.unab.identiy.identitysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.unab.identiy.identitysql.domain.Administrador;


@Repository
public interface AdministradorRepository extends JpaRepository<Administrador, String>{

}