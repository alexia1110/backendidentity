package cl.unab.identiy.identitysql.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.unab.identiy.identitysql.domain.ParticipanteActividad;

@Repository
public interface ParticipanteActividadRepository extends JpaRepository<ParticipanteActividad, Integer> {

    //AlumnoActividad findById_alumnoAndId_actividad(String id_alumno, Long id_actividad);
 
    Optional<ParticipanteActividad> findByIdparticipanteAndIdactividad(String idparticipante, Long idactividad);
    List<ParticipanteActividad> findByIdparticipante(String idparticipante);
  
}