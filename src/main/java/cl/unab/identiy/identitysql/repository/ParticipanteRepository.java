package cl.unab.identiy.identitysql.repository;

import java.util.Optional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cl.unab.identiy.identitysql.domain.Participante;

@Repository
public interface ParticipanteRepository extends JpaRepository<Participante,String>{
    
}