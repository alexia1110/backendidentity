package cl.unab.identiy.identitysql.security;

/**
 * Constants for Spring Security authorities.
 * @author Axity
 * @version 1.0
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
