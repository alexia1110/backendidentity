package cl.unab.identiy.identitysql.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import cl.unab.identiy.identitysql.domain.Actividad;
import cl.unab.identiy.identitysql.repository.ActividadRepository;
import cl.unab.identiy.identitysql.service.ActividadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class ActividadImpl implements ActividadService {

    private final Logger log = LoggerFactory.getLogger(ActividadImpl.class);
    @Autowired
    private final ActividadRepository activityRepository;
    private final Map hintMap = new HashMap<>();
    private final int size = 350;

    public ActividadImpl(ActividadRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public Actividad save(Actividad activity) {
        log.debug("Request to save Usuario : {}", activity);
        String qrCode = "{'name':'"+activity.getNombreactividad()+"', 'date':'"+activity.getFecha()+"', 'horaInicio':'"+activity.getHoraInit()+"', 'HoraFin':'"+activity.getHoraEnd()+"', 'sala':'"+activity.getSala()+"', 'description':'"+activity.getDescription()+"'}";
        try {
            
            activityRepository.save(activity);
            
            String res = createQRCode(qrCode);
            activity.setCodeqr(res);
            log.info(activity.toString());
           // ActivityListAlumno activityListAlumnoDTO = new ActivityListAlumno();
           // activityListAlumnoDTO.setCupoDisponible(activity.getCupo());
           // activityListAlumnoDTO.setCodeActivity(activity.getId());
        }catch (Exception e) {
            //TODO: handle exception
        }      
        return activityRepository.save(activity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Actividad> findAll() {
        log.debug("Request to get all Usuarios");
        return activityRepository.findAll();
    }


    /**
     * Get one usuario by id.
     *
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Actividad> findById(Long id) {
        log.debug("Request to get Usuario : {}", id);
        return activityRepository.findById(id);
    }


    @Override
    public void delete(Long id) {
        log.debug("Request to delete Usuario : {}", id);
        activityRepository.deleteById(id);
    }
   
    @Override
    public boolean exist(Long id) {
        return activityRepository.existsById(id);
    }

    public String createQRCode(String qrCodeData) throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(qrCodeData, BarcodeFormat.QR_CODE, size, size, hintMap);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(matrix, "png", baos);
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

}