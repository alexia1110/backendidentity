package cl.unab.identiy.identitysql.service;

import java.util.List;
import java.util.Optional;

import cl.unab.identiy.identitysql.domain.Administrador;

public interface AdministradorService {
    
    Administrador save(Administrador administrador);

    List<Administrador> findAll();

    Optional<Administrador> findOne(String rut);
    
    boolean exists(String rut);

    void delete(String rut);
}