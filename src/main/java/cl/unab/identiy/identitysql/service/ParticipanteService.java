package cl.unab.identiy.identitysql.service;

import cl.unab.identiy.identitysql.domain.Participante;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ParticipanteService {

    Participante save(Participante alumno);

    List<Participante> findAll();

    Optional<Participante> findOne(String rut);
    
    boolean exists(String rut);

    void delete(String rut);
}