package cl.unab.identiy.identitysql.service.impl;

import cl.unab.identiy.identitysql.domain.ParticipanteActividad;
import cl.unab.identiy.identitysql.repository.ParticipanteActividadRepository;
import cl.unab.identiy.identitysql.service.ParticipanteActividadService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class ParticipanteActividadImpl implements ParticipanteActividadService {

    private final Logger log = LoggerFactory.getLogger(ParticipanteActividadImpl.class);
    @Autowired
    private final ParticipanteActividadRepository participanteActividadRepository;

    public ParticipanteActividadImpl(ParticipanteActividadRepository participanteActividadRepository) {
        this.participanteActividadRepository = participanteActividadRepository;
    }

    @Override
    public ParticipanteActividad save(ParticipanteActividad participanteActividad) {
        log.debug("Request to save AlumnoDTO : {}", participanteActividad);
        return participanteActividadRepository.save(participanteActividad);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ParticipanteActividad> findAll() {
        log.debug("Request to get all Usuarios");
        return participanteActividadRepository.findAll();
    }

    @Override
    public void deleteByAlumnoAndActivity(String rut, Long id) {
        log.info("Request to delete TaHtcTrn : {}", rut);
        log.info(participanteActividadRepository.findByIdparticipanteAndIdactividad(rut, id).toString());
        Optional<ParticipanteActividad>  alumnoActividad = participanteActividadRepository.findByIdparticipanteAndIdactividad(rut, id);
        
        participanteActividadRepository.deleteById(alumnoActividad.get().getId());
    }


    @Override
    public List<ParticipanteActividad> findByIdalumno(String rut) {
        log.info("Request to delete TaHtcTrn : {}", rut);
     return participanteActividadRepository.findByIdparticipante(rut);
    }
    

}