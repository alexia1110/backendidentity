package cl.unab.identiy.identitysql.service;

import java.util.List;
import java.util.Optional;

import cl.unab.identiy.identitysql.domain.ParticipanteActividad;

public interface ParticipanteActividadService {

    ParticipanteActividad save(ParticipanteActividad alumnoActividad);
    
    List<ParticipanteActividad> findAll();

    void deleteByAlumnoAndActivity(String rut,Long id);

    List<ParticipanteActividad> findByIdalumno(String rut);


}