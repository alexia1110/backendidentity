package cl.unab.identiy.identitysql.service;

import java.util.List;
import java.util.Optional;

import cl.unab.identiy.identitysql.domain.Actividad;

public interface ActividadService {

    Actividad save(Actividad activity);

    List<Actividad> findAll();

    Optional<Actividad> findById(Long id);

    void delete(Long id);

    boolean exist(Long id);

}