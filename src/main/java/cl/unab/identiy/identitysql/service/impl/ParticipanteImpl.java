package cl.unab.identiy.identitysql.service.impl;

import cl.unab.identiy.identitysql.domain.Participante;
import cl.unab.identiy.identitysql.repository.ParticipanteRepository;
import cl.unab.identiy.identitysql.service.ParticipanteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ParticipanteImpl implements ParticipanteService {
    private final Logger log = LoggerFactory.getLogger(ParticipanteImpl.class);

    @Autowired
    private final ParticipanteRepository participanteRepository;
   

    public ParticipanteImpl(ParticipanteRepository participanteRepository) {
        this.participanteRepository = participanteRepository;
    }

    @Override
    public Participante save(Participante participante) {
        log.debug("Request to save AlumnoDTO : {}", participante);
        return participanteRepository.save(participante);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Participante> findAll() {
        log.debug("Request to get all TaHtcTrns");
        return participanteRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Participante> findOne(String rut) {
        log.debug("Request to get TaHtcTrn : {}", rut);
        return participanteRepository.findById(rut);
    }

    @Override
    public void delete(String rut) {
        log.debug("Request to delete TaHtcTrn : {}", rut);
        participanteRepository.deleteById(rut);
    }

    @Override
    public boolean exists(String rut) {
        return participanteRepository.existsById(rut);
    }
}