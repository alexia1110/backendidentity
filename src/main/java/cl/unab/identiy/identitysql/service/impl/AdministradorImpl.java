package cl.unab.identiy.identitysql.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.unab.identiy.identitysql.domain.Administrador;
import cl.unab.identiy.identitysql.repository.AdministradorRepository;
import cl.unab.identiy.identitysql.service.AdministradorService;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class AdministradorImpl implements AdministradorService{

    private final Logger log = LoggerFactory.getLogger(AdministradorImpl.class);

    @Autowired
    private final AdministradorRepository administradorRepository;

    public AdministradorImpl(AdministradorRepository administradorRepository) {
        this.administradorRepository = administradorRepository;
    }

    @Override
    public Administrador save(Administrador administrador) {
        log.debug("Request to save AlumnoDTO : {}", administrador);
        return administradorRepository.save(administrador);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Administrador> findAll() {
        log.debug("Request to get all TaHtcTrns");
        return administradorRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Administrador> findOne(String rut) {
        log.debug("Request to get TaHtcTrn : {}", rut);
        return administradorRepository.findById(rut);
    }

    @Override
    public void delete(String rut) {
        log.debug("Request to delete TaHtcTrn : {}", rut);
        administradorRepository.deleteById(rut);
    }

    @Override
    public boolean exists(String rut) {
        return administradorRepository.existsById(rut);
    }
}