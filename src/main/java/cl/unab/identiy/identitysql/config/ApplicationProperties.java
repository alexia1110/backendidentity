package cl.unab.identiy.identitysql.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Concepto.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link} for a good example. */

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

}


