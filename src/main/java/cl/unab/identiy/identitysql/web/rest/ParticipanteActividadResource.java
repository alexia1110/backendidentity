package cl.unab.identiy.identitysql.web.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import cl.unab.identiy.identitysql.domain.ParticipanteActividad;
import cl.unab.identiy.identitysql.service.ParticipanteActividadService;
import cl.unab.identiy.identitysql.web.rest.error.BadRequestAlertException;
import cl.unab.identiy.identitysql.web.rest.util.HeaderUtil;
import cl.unab.identiy.identitysql.web.rest.util.PaginationUtil;
import cl.unab.identiy.identitysql.web.rest.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*") 
public class ParticipanteActividadResource {

    private final Logger log = LoggerFactory.getLogger(ParticipanteActividadResource.class);

    private static final String ENTITY_NAME = "participanteactividad";

    private static final String applicationName = "unab";

    private  ParticipanteActividadService participanteActividadService;

    public ParticipanteActividadResource(ParticipanteActividadService participanteActividadService) {
        this.participanteActividadService = participanteActividadService;
    }

    @PostMapping("/participante-activity")
     public ResponseEntity<ParticipanteActividad> createAlumnoActivity(@Valid @RequestBody ParticipanteActividad participante) throws URISyntaxException {
         log.debug("REST request to save Usuario : {}", participante);
         log.info(participante.toString());
         ParticipanteActividad result = participanteActividadService.save(participante);
         return ResponseEntity.created(new URI("/api/alumnoactivity/" + result.getId()))
             .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
             .body(result);
     }

     @GetMapping("/participante-activity")
     public ResponseEntity<List<ParticipanteActividad>> getAllAlumno() {
         log.debug("REST request to get a page of activities");
         List<ParticipanteActividad> result = participanteActividadService.findAll();
         return ResponseEntity.ok()
             .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  String.valueOf(result.size())))
             .body(result);
     }

     @DeleteMapping("/participante-activity/{rut}/{id}")
     public ResponseEntity<Void> deleteactivity(@PathVariable Long id,@PathVariable String rut) {
         log.debug("REST request to delete activity : {}", id);
         participanteActividadService.deleteByAlumnoAndActivity(rut, id);
         return ResponseEntity.ok()
         .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, rut)).build();
     }


     @GetMapping("/participante-activity/{rut}")
     public ResponseEntity<List<ParticipanteActividad>> getAllActivityByAlumno(@PathVariable String rut) {
         log.debug("REST request to get a page of activities");
         List<ParticipanteActividad> result = participanteActividadService.findByIdalumno(rut);
         return ResponseEntity.ok()
             .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  String.valueOf(result.size())))
             .body(result);
     }
}