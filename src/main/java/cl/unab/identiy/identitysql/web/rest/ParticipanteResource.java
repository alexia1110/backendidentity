package cl.unab.identiy.identitysql.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import cl.unab.identiy.identitysql.domain.Participante;
import cl.unab.identiy.identitysql.repository.ParticipanteRepository;
import cl.unab.identiy.identitysql.service.ParticipanteService;
import cl.unab.identiy.identitysql.web.rest.error.BadRequestAlertException;
import cl.unab.identiy.identitysql.web.rest.util.HeaderUtil;
import cl.unab.identiy.identitysql.web.rest.util.PaginationUtil;
import cl.unab.identiy.identitysql.web.rest.util.ResponseUtil;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*") 
public class ParticipanteResource {

    private final Logger log = LoggerFactory.getLogger(ParticipanteResource.class);

    private static final String ENTITY_NAME = "participante";

    private static final String applicationName = "unab";

    @Autowired
    private final ParticipanteRepository participanteRepository;

    private final ParticipanteService participanteService;


    public ParticipanteResource(ParticipanteService participanteService, ParticipanteRepository participanteRepository) {
        this.participanteService = participanteService;
        this.participanteRepository = participanteRepository;
    }
    
    @PostMapping("/alumno")
     public ResponseEntity<Participante> createAlumno(@Valid @RequestBody Participante participante) throws URISyntaxException {
         log.info("REST request to save Usuario : {}", participante);
        if (participanteRepository.existsById(participante.getRut())) {
             throw new BadRequestAlertException("Ya existe una actividad con el id", ENTITY_NAME, "idexists");
        }
        Participante result = participanteRepository.save(participante);
     log.info(result.toString());
         return ResponseEntity.created(new URI("/api/alumno/" + result.getRut()))
             .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getRut().toString()))
             .body(result);
     }

     @PutMapping("/alumno")
     public ResponseEntity<Participante> updateaActivity(@Valid @RequestBody Participante participante) throws URISyntaxException {
         log.debug("REST request to update activity : {}", participante);
         if (participante.getRut() == null) {
             throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
         }
         Participante result = participanteService.save(participante);
         return ResponseEntity.ok()
             .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, participante.getRut().toString()))
             .body(result);
     }

     @GetMapping("/alumno")
     public ResponseEntity<List<Participante>> getAllAlumno() {
         log.debug("REST request to get a page of activities");
         List<Participante> result = participanteRepository.findAll();
         return ResponseEntity.ok()
             .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  String.valueOf(result.size())))
             .body(result);
     }


         /**
     * {@code GET  /alumno/:rut} : get the "rut" activity.
     *
     * @param rut the id of the activityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the activityDTO, or with status {@code 404 (Not Found)}
     * **/
    @GetMapping("/alumno/{rut}")
    public ResponseEntity<Participante> getAlumnoById(@PathVariable String rut) {
        log.debug("REST request to get activity : {}", rut);
        Optional<Participante> participante = participanteService.findOne(rut);
        return ResponseUtil.wrapOrNotFound(participante);
    } 


    /**
     * {@code DELETE  /alumno/:rut} : delete the "rut" activity.
     *
     * @param id the id of the activityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/alumno/{id}")
    public ResponseEntity<Void> deleteAlumno(@PathVariable String rut) {
        log.debug("REST request to delete activity : {}", rut);
        participanteService.delete(rut);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, rut)).build();
    }


    
}