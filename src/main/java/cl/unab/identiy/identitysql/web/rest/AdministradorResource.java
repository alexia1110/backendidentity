package cl.unab.identiy.identitysql.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import cl.unab.identiy.identitysql.domain.Administrador;
import cl.unab.identiy.identitysql.service.AdministradorService;
import cl.unab.identiy.identitysql.web.rest.util.HeaderUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*") 
public class AdministradorResource {

    private final Logger log = LoggerFactory.getLogger(AdministradorResource.class);

    private static final String ENTITY_NAME = "administrador";

    private static final String applicationName = "unab";

    private  AdministradorService administradorService;

    public AdministradorResource(AdministradorService administradorService) {
        this.administradorService = administradorService;
    }

    @PostMapping("/administrador")
    public ResponseEntity<Administrador> createAlumnoActivity(@Valid @RequestBody Administrador activity) throws URISyntaxException {
        log.debug("REST request to save Usuario : {}", activity);
        log.info(activity.toString());
        Administrador result = administradorService.save(activity);
        return ResponseEntity.created(new URI("/api/alumnoactivity/" + result.getRut()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getRut().toString()))
            .body(result);
    }

    @GetMapping("/administrador")
    public ResponseEntity<List<Administrador>> getAllAlumno() {
        log.debug("REST request to get a page of activities");
        List<Administrador> result = administradorService.findAll();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  String.valueOf(result.size())))
            .body(result);
    }

    @DeleteMapping("/administrador/{rut}")
    public ResponseEntity<Void> deleteAdministrador(@PathVariable String rut) {
        log.debug("REST request to delete activity : {}", rut);
        administradorService.delete(rut);
        return ResponseEntity.ok()
        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, rut)).build();
    }


    @GetMapping("/administrador/{rut}")
    public ResponseEntity<Optional<Administrador>> getAdministradorById(@PathVariable String rut) {
        log.debug("REST request to get a page of activities");
        Optional<Administrador> result = administradorService.findOne(rut);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  result.get().getRut()))
            .body(result);
    }

}