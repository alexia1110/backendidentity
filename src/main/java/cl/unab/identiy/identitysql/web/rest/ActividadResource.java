package cl.unab.identiy.identitysql.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import cl.unab.identiy.identitysql.domain.Actividad;
import cl.unab.identiy.identitysql.service.ActividadService;
import cl.unab.identiy.identitysql.web.rest.error.BadRequestAlertException;
import cl.unab.identiy.identitysql.web.rest.util.HeaderUtil;
import cl.unab.identiy.identitysql.web.rest.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ActividadResource {

    private final Logger log = LoggerFactory.getLogger(ActividadResource.class);

    private static final String ENTITY_NAME = "alumno";

    private static final String applicationName = "unab";

    private final ActividadService activityService;

    public ActividadResource(ActividadService activityService) {
        this.activityService = activityService;
    }

    @PostMapping("/actividad")
    public ResponseEntity<Actividad> createAlumno(@Valid @RequestBody Actividad activiy) throws URISyntaxException {
        log.info("REST request to save Usuario : {}", activiy);
       if (activityService.exist(activiy.getIdActivity())) {
            throw new BadRequestAlertException("Ya existe una actividad con el id", ENTITY_NAME, "idexists");
       }
       Actividad result = activityService.save(activiy);
    log.info(result.toString());
        return ResponseEntity.created(new URI("/api/alumno/" + Long.toString(result.getIdActivity())))
        .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  Long.toString(result.getIdActivity())))
            .body(result);
    }

    @PutMapping("/actividad")
    public ResponseEntity<Actividad> updateaActivity(@Valid @RequestBody Actividad activiy) throws URISyntaxException {
        log.debug("REST request to update activity : {}", activiy);
        if (activiy.getIdActivity() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Actividad result = activityService.save(activiy);
        return ResponseEntity.ok()
        .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  Long.toString(result.getIdActivity())))
            .body(result);
    }

    @GetMapping("/actividad")
    public ResponseEntity<List<Actividad>> getAllAlumno() {
        log.debug("REST request to get a page of activities");
        List<Actividad> result = activityService.findAll();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,  String.valueOf(result.size())))
            .body(result);
    }


    
   @GetMapping("/actividad/{id}")
   public ResponseEntity<Actividad> getAlumnoById(@PathVariable Long id) {
       log.debug("REST request to get activity : {}", id);
       Optional<Actividad> activiy = activityService.findById(id);
       return ResponseUtil.wrapOrNotFound(activiy);
   } 


 
   @DeleteMapping("/actividad/{id}")
   public ResponseEntity<Void> deleteAlumno(@PathVariable Long id) {
       log.debug("REST request to delete activity : {}", id);
       activityService.delete(id);
       return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, Long.toString(id))).build();
   }

}