package cl.unab.identiy.identitysql.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "actividad")
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Actividad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long idActivity;

    @Column(name = "nombreactividad")
    private String nombreactividad;

    @Column(name = "idCreador")
    private String idCreador;

    @Column(name = "idestado")
    private Integer idestado;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'")
    @Column(name = "fecha", nullable = false)
    private String fecha;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="T'HH:mm")
    @Column(name = "horaInit", nullable = false)
    private String horaInit;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="T'HH:mm")
    @Column(name = "horaEnd", nullable = false)
    private String horaEnd;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm")
    @Column(name = "fechaCreacion", nullable = false)
    private String fechaCreacion;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm")
    @Column(name = "fechaActualizacion", nullable = true)
    private String fechaActualizacion;

    @Column(name = "carrera")//array¿?
    private String carrera;

    @Column(name = "sala", nullable = false)
    private String sala;


    @Column(name = "cupo", nullable = false)
    private Number cupo;

 
    @Column(name = "codeqr", nullable = true, length=10485760)
    private String codeqr;


    @Column(name = "description", nullable = false)
    private String description;

    public Long getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Long idActivity) {
        this.idActivity = idActivity;
    }

    public String getNombreactividad() {
        return nombreactividad;
    }

    public void setNombreactividad(String nombreactividad) {
        this.nombreactividad = nombreactividad;
    }


    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }


    public Integer getEstado() {
        return idestado;
    }

    public void setEstado(Integer idestado) {
        this.idestado = idestado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInit() {
        return horaInit;
    }

    public void setHoraInit(String horaInit) {
        this.horaInit = horaInit;
    }

    public String getHoraEnd() {
        return horaEnd;
    }

    public void setHoraEnd(String horaEnd) {
        this.horaEnd = horaEnd;
    }


    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public Number getCupo() {
        return cupo;
    }

    public void setCupo(Number cupo) {
        this.cupo = cupo;
    }

    public String getCodeqr() {
        return codeqr;
    }

    public void setCodeqr(String codeqr) {
        this.codeqr = codeqr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(String idCreador) {
        this.idCreador = idCreador;
    }

    public Integer getIdestado() {
        return idestado;
    }

    public void setIdestado(Integer idestado) {
        this.idestado = idestado;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Actividad)) {
            return false;
        }
        return idActivity != null && idActivity.equals(((Actividad) o).idActivity);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Actividad{" + "idActivity='" + getIdActivity() + "'" + ", nombreactividad='" + getNombreactividad() + "'" +  ", creador='" + getIdCreador() + "'" + ", estado='" + getEstado() + "'" +  ", fecha='" + getFecha() + "'" +  ", horaInit='" + getHoraInit() + "'" +", horaEnd='" + getHoraEnd() + "'" +", carrera='" + getCarrera() + "'" + ", cupo='" + getCupo() + "'" + ", description='" + getDescription() + "'" +", codeqr='" + getCodeqr() + "'" +", fechaCreacion='" + getFechaCreacion() +"'" +", fechaActualizacion='" + getFechaActualizacion() + "'" +"}";
    }


    


}