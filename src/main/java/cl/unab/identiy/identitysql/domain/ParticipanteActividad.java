package cl.unab.identiy.identitysql.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "participanteactividad")
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ParticipanteActividad {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @Column(name = "idparticipante", nullable = false)
    private String idparticipante;

    @Column(name = "idactividad", nullable = false)
    private Long idactividad;

    @Column(name = "asistencia", nullable = false)
    private Boolean asistencia;

    @Column(name = "comentario", nullable = true)
    private String comentario;

    @Column(name = "jsonCertificado", nullable = true)
    private String jsonCertificado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getidactividad() {
        return idactividad;
    }

    public void setidactividad(Long idactividad) {
        this.idactividad = idactividad;
    }

    public Boolean getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(Boolean asistencia) {
        this.asistencia = asistencia;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getIdparticipante() {
        return idparticipante;
    }

    public void setIdparticipante(String idparticipante) {
        this.idparticipante = idparticipante;
    }

    public Long getIdactividad() {
        return idactividad;
    }

    public void setIdactividad(Long idactividad) {
        this.idactividad = idactividad;
    }

    public String getJsonCertificado() {
        return jsonCertificado;
    }

    public void setJsonCertificado(String jsonCertificado) {
        this.jsonCertificado = jsonCertificado;
    }

    @Override
    public String toString() {
        return "ParticipanteActividad [asistencia=" + asistencia + ", comentario=" + comentario + ", id=" + id
                + ", idactividad=" + idactividad + ", idparticipante=" + idparticipante + ", jsonCertificado="
                + jsonCertificado + "]";
    }



   

    



}