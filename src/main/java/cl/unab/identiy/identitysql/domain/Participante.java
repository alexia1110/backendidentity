package cl.unab.identiy.identitysql.domain;

import java.io.Serializable;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "participante")
@EntityListeners(AuditingEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Participante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String rut;

    @Column(name = "idTipo",nullable = false)
    private String idTipo;

    @Column(name = "idEstadoAcademico", nullable = true)
    private String idEstadoAcademico;

    @Column(name = "password",nullable = false)
    private String password;

    @Column(name = "nombre",nullable = false)
    private String nombre;

    @Column(name = "apellido",nullable = false)
    private String apellido;

    @Column(name = "idCarrera",nullable = true)
    private String idCarrera;

    @Column(name = "correo" ,nullable = false)
    private String correo;

    @Column(name = "contacto",nullable = true)
    private String contacto;

    @Column(name = "curriculum", nullable = true, length=10485760)
    private String curriculum;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getidEstadoAcademico() {
        return idEstadoAcademico;
    }

    public void setidEstadoAcademico(String idEstadoAcademico) {
        this.idEstadoAcademico = idEstadoAcademico;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(String idCarrera) {
        this.idCarrera = idCarrera;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Participante)) {
            return false;
        }
        return rut != null && rut.equals(((Participante) o).rut);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Participante [apellido=" + apellido + ", contacto=" + contacto + ", correo=" + correo + ", curriculum="
                + curriculum + ", idCarrera=" + idCarrera + ", idEstadoAcademico=" + idEstadoAcademico + ", idTipo="
                + idTipo + ", nombre=" + nombre + ", password=" + password + ", rut=" + rut + "]";
    }





}